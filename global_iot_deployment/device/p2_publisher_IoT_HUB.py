import json
import sys
import time

from azure.iot.device import IoTHubDeviceClient, Message
from p2_device_telemetry import get_telemetry

# Reading credential from a local file not uploaded to public repository

try:
    with open(
            "D:\\ELASTACLOUD\\iot_hub_credentials\\publisher_config_IoT_HUB.json",
            "r") as f:
        configuration = json.loads(f.read())
except:
    print("ERROR: Configuration file not found.")
    sys.exit("Stopping program. Reason: Fatal ERROR.")

CONNECTION_STRING = "HostName=" + configuration["HostName"] + ";DeviceId=" + \
                    configuration["DeviceId"] + ";SharedAccessKey=" + \
                    configuration["SharedAccessKey"]

if __name__ == '__main__':
    try:
        client = IoTHubDeviceClient.create_from_connection_string(
            CONNECTION_STRING)
        print("IoT Hub device sending periodic messages, press Ctrl-C to exit")

        while True:
            # Build the message with simulated telemetry values.
            message = Message(get_telemetry())
            # Send the message each 5 seconds.
            client.send_message(message)
            print("Message successfully sent")
            time.sleep(5)

    except KeyboardInterrupt:
        print("IoTHubClient sample stopped")
