import json
import os.path
import sys
import time
from os import path

import paho.mqtt.client as mqtt
from p2_device_telemetry import get_telemetry

# this is because I wanted to run manually each script and also to use Start.bat
relative_path = "C:\\Users\\Max\\PycharmProjects\\elastacloud_stage_2\\global_iot_deployment\\config_and_credentials"

if path.exists(relative_path) is False:
    print("ERROR: Configuration folder not found.")
    sys.exit("Stopping program. Reason: Fatal ERROR.")

os.chdir(relative_path)

if __name__ == "__main__":
    # loading configuration file
    # avoiding hardcoded variables --> same code uses different configurations

    try:
        with open("config_v01.json", "r") as f:
            configuration = json.loads(f.read())
    except:
        print("ERROR: Configuration file not found.")
        sys.exit("Stopping program. Reason: Fatal ERROR.")


    # The callback for when the client receives a CONNACK response from the server.
    def on_connect(client, userdata, flags, rc):
        print("Connected with code " + str(rc))


    # The callback for when a PUBLISH message is received from the server.
    def on_publish(client, userdata, mid):
        print("Published: " + str(mid))


    client = mqtt.Client()
    # Overwriting what I want to do on_connect and on_publish
    client.on_connect = on_connect
    client.on_publish = on_publish
    client.will_set(configuration['will_topic'],
                    "disconnected!")  # I use an unique topic
    client.connect(configuration['broker'], configuration['port'], 60)
    client.loop_start()

    while True:
        # Sending data
        # QoS Quality of Service 2 means that Messages arrive exactly once
        # Safest and slowest (NEVER TRUST A PROVIDER/SUPPLIER. FROM MY EXPERIENCE)
        client.publish(configuration['topic'], get_telemetry(), qos=2,
                       retain=False)  # I use an unique topic
        time.sleep(configuration['sending_interval'])

    client.disconnect()

# after finishing Ctrl+Alt+L for nicer autoformat
