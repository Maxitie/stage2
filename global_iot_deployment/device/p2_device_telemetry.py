import json
import os.path
import random
import sys
from os import path

# this is because I wanted to run manually each script and also to use Start.bat
relative_path = "C:\\Users\\Max\\PycharmProjects\\elastacloud_stage_2\\global_iot_deployment\\config_and_credentials"

if path.exists(relative_path) is False:
    print("ERROR: Configuration folder not found.")
    sys.exit("Stopping program. Reason: Fatal ERROR.")

os.chdir(relative_path)

# Simulating a reasonable generation of device battery level.
battery_lvl = 1


def get_battery_lvl():
    global battery_lvl
    # this gives a slighly lesser values for battery
    battery_lvl = round(random.uniform(battery_lvl - 0.05, battery_lvl), 3)
    if battery_lvl < 0.05:
        battery_lvl = 0
    return float(battery_lvl)


# Generate bounded realistic but random values for Latitude and Longitude
# rounding to 6 decimals as used by google maps.
def get_long():
    return float(round(random.uniform(-2.5, -2.3), 6))


def get_lat():
    return float(round(random.uniform(36.81, 36.89), 6))


# I guess Device ID and Name would be stored in a file on devices memory.
# To simulate that I will create a JSON file with that data.
# Same script installed on any device will get its unique credentials.
# Getting my credentials and storing them in a dictionary
# this will load from memory just once when get_telemetry is loaded
try:
    with open("device_credentials.json", "r") as f:
        credentials = json.loads(f.read())
except:
    print("ERROR: Device credentials file not found.")
    sys.exit("Stoping program. Reason: Fatal ERROR.")

device_id = credentials['ID']
device_name = credentials['Name']


def get_telemetry():
    # Preparing data
    telemetry = {}
    telemetry['ID'] = device_id
    telemetry['Name'] = device_name
    telemetry['Longitude'] = get_long()
    telemetry['Latitude'] = get_lat()
    telemetry['Battery_lvl'] = get_battery_lvl()
    # It would be interesting to include timestamp as part of the telemetry

    return json.dumps(telemetry, default=str)

# after finishing Ctrl+Alt+L for nicer autoformat
