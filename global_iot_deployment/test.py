# Installed:
# pip3 install pycryptodome
# pip3 install simple-crypt --no-dependencies



from simplecrypt import encrypt, decrypt

ciphertext = encrypt('123', "test")

print(ciphertext)

plaintext = decrypt('123', ciphertext)

print(plaintext)
