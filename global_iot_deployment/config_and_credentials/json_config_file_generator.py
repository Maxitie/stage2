import json
import os.path

os.chdir(
    "/global_iot_deployment/config_and_credentials")

config = {}
config['will_topic'] = "max_stage2/disconection"
config['topic'] = "max_stage2/msg"
config['broker'] = "broker.hivemq.com"
config['port'] = 1883
config['sending_interval'] = 5

# Save the credentials object to file
with open("config_v01.json", "w") as file:
    json.dump(config, file)
