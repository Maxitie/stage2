import json
import os
import os.path

os.chdir(
    "/global_iot_deployment/config_and_credentials")

credentials = {}
credentials['ID'] = "Device_ID"
credentials['Name'] = "Device_Name"

# Save the credentials object to file
with open("device_credentials.json", "w") as file:
    json.dump(credentials, file)
