import json
import sys
import time
from datetime import date, datetime
from os import path
from pathlib import Path

import paho.mqtt.client as mqtt
import pandas as pd
import pyarrow as pa
import pyarrow.parquet as pq

if __name__ == "__main__":
    json_list = []

    if path.exists("D:") is False:
        print("ERROR: Hard Drive not found.")
        sys.exit("Stopping program. Reason: Fatal ERROR.")


    def json_validator(my_json):
        if len(my_json) == 5:
            try:
                isinstance(my_json["ID"], str)
            except:
                print("Error on ID")
                return False
            try:
                isinstance(my_json["Name"], str)
            except:
                print("Error on Name")
                return False
            try:
                isinstance(my_json["Longitude"], float)
            except:
                print("Error on Longitude")
                return False
            try:
                isinstance(my_json["Latitude"], float)
            except:
                print("Error Latitude")
                return False
            try:
                isinstance(my_json["Battery_lvl"], float)
            except:
                print("Error Battery_lvl")
                return False
            return True


    def create_parquet(json_list):
        df_to_dump = pd.DataFrame.from_records(json_list)
        # Convert DataFrame to Apache Arrow Table
        table = pa.Table.from_pandas(df_to_dump)
        # creates a folder if there is no folder, I will create a folder each day
        Path("D:\\stage2\\stage2_parkets\\" + str(date.today())).mkdir(
            parents=True,
            exist_ok=True)
        # Parquet with Brotli compression with date and time
        pq.write_table(table,
                       'D:\\stage2\\stage2_parkets\\' + str(
                           date.today()) + "\\" + str(
                           datetime.now().strftime(
                               "%d-%m-%Y_%H-%M-%S")) + '.parquet')
        print("Saved .parquet")


    def on_connect(client, userdata, flags, rc):
        print("Connected with code " + str(rc))
        # after connection I will subscribe to a topic and start reciebing data.
        client.subscribe("max_stage2/msg")


    def on_message(unused_client, unused_userdata, message):
        global json_list
        # getting the payload and encoding to utf-8 as string
        telemetry = json.loads(str(message.payload, "utf-8"))
        if json_validator(telemetry):
            json_list.append(telemetry)


    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
    client.connect("broker.hivemq.com", 1883, 60)
    client.loop_start()

    while True:
        # with this if I am avoiding the creation of an empty file
        if len(json_list) > 0:
            # Visualising
            print(json_list)
            parallel_list = list(json_list)
            create_parquet(parallel_list)
            # in order to not loose data while saving I will delete saved data from
            # the list after saving
            json_list = json_list[len(parallel_list):]

        # for testing purposes I group telemetrys in batches of each 20 seconds
        time.sleep(20)

    client.disconnect()

# after finishing Ctrl+Alt+L for nicer autoformat
