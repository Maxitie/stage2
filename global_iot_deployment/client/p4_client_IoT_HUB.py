import json
import sys
from datetime import date, datetime
from pathlib import Path

import pandas as pd
import pyarrow as pa
import pyarrow.parquet as pq
from azure.eventhub import EventHubConsumerClient

# Reading credential from a local file not uploaded to public repository
try:
    with open(
            "D:\\ELASTACLOUD\\iot_hub_credentials\\client_config_IoT_HUB.json",
            "r") as f:
        configuration = json.loads(f.read())

except:
    print("ERROR: Configuration file not found.")
    sys.exit("Stopping program. Reason: Fatal ERROR.")

CONNECTION_STR = "Endpoint=" + configuration[
    "Endpoint"] + ";SharedAccessKeyName=iothubowner;SharedAccessKey=" + \
                 configuration["SharedAccessKey"]

json_list = []


def create_parquet(json_list):
    df_to_dump = pd.DataFrame.from_records(json_list)
    # Convert DataFrame to Apache Arrow Table
    table = pa.Table.from_pandas(df_to_dump)
    # creates a folder if there is no folder, I will create a folder each day
    Path("D:\\stage2\\stage2_parkets_IoT_HUB\\" + str(date.today())).mkdir(
        parents=True,
        exist_ok=True)
    # Parquet with Brotli compression with date and time
    pq.write_table(table,
                   'D:\\stage2\\stage2_parkets_IoT_HUB\\' + str(
                       date.today()) + "\\" + str(
                       datetime.now().strftime(
                           "%d-%m-%Y_%H-%M-%S")) + '.parquet')
    print("Saved .parquet")


# Define callbacks to process events
def on_event_batch(partition_context, events):
    for event in events:
        print("Telemetry received: ", event.body_as_str())

        global json_list

        json_list.append(json.loads(event.body_as_str()))
        # Lets save data each 10 msgs
        if len(json_list) > 10:
            parallel_list = list(json_list)
            create_parquet(parallel_list)
            json_list = json_list[len(parallel_list):]

    partition_context.update_checkpoint()


if __name__ == '__main__':
    client = EventHubConsumerClient.from_connection_string(
        conn_str=CONNECTION_STR,
        consumer_group="$default", )
    try:
        with client:
            client.receive_batch(on_event_batch=on_event_batch)

    except KeyboardInterrupt:
        print("Receiving has stopped.")
