import sys
from os import path
from pyspark.sql import SparkSession
from pyspark.sql import functions
from pyspark.sql.types import StructType, StructField, StringType, DoubleType


def main(directory) -> None:
    # starting a Spark Session
    spark = SparkSession \
        .builder \
        .master("local[2]") \
        .appName("Testing_Functionality") \
        .getOrCreate()

    # definying the structure
    fields = [StructField("ID", StringType(), True),
              StructField("Name", StringType(), True),
              StructField("Longitude", DoubleType(), True),
              StructField("Latitude", DoubleType(), True),
              StructField("Battery_lvl", DoubleType(), True), ]

    # Create DataFrame representing the stream of input files from our devices
    lines = spark \
        .readStream \
        .format('parquet') \
        .schema(StructType(fields)) \
        .load(directory)

    # lines.printSchema()

    # Compute a mean value for longitude and latitude
    values = lines \
        .groupBy(lines.ID) \
        .agg(functions.mean("Latitude").alias("mean_Latitude"),
             functions.mean("Longitude").alias("mean_Longitude"))

    # values.printSchema()

    # Start running the query that prints the output on the screen
    query = values \
        .writeStream \
        .outputMode("complete") \
        .format("console") \
        .start()

    query.awaitTermination()


if __name__ == '__main__':
    # This script will execute on both data folders unles given a route to a specific folder
    if len(sys.argv) != 2 and path.exists("D:\\stage2"):
        print("Executing spark on Both Folders")
        # Adding "\\**\\*" allows to load from all subfolders
        main("D:\\stage2" + "\\**\\*")
    else:
        print("Executing spark on given folder.")
        if len(sys.argv) == 2 and path.exists(sys.argv[1]):
            main(sys.argv[1])
        else:
            print("ERROR: Path does not exist.")
            sys.exit("Stopping program. Reason: Fatal ERROR.")

# after finishing Ctrl+Alt+L for nicer autoformat
